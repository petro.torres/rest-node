require('./config/config');

console.log(autor);

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());
 
app.get('/usuario', function (req, res) {
  
  res.json({nombre: "petro", peticion: "get", valor: "usuario"});
})

app.post('/usuario', function (req, res) {
    //res.send('Hello World');
    let body = req.body;
    if(body.nombre === undefined){
        res.status(400).json({ok: false, datos:body});
    }else{
        res.json({
            datos_persona:body
        })
    }
    
  })

  app.put('/usuario/:id', function (req, res) {
    //res.send('Hello World');
    let idp = req.params.id;
    res.json({nombre: "petro", peticion: "put", id: idp});
  })

  app.delete('/usuario', function (req, res) {
    //res.send('Hello World');
    res.json({nombre: "petro", peticion: "delete"});
  })
 
app.listen(process.env.PORT, () => {
    console.log(`Corriendo en el puerto 3000`);
})